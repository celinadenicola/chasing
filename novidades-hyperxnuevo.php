<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
            

            

                <!--<div class="col-md-12 fondo novidades-boxes3b">
                <div class="col-md-10">
                <h2>As novidades de HyperX na BGS 2015</h2>
                <p>HyperX lança novo headset Cloud Core na Brasil Gamer Show 2015 e demonstra a potência dos SSDs e Memórias voltado para gamers.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='chasing-better.php'">Conheça mais</button>
             
              </div>-->


                 <div class="col-md-12 fondo">
                <div class="col-md-12">
               <div class="col-md-4 padding-right"><img src="images/productos/headsets.png" class="img-responsive"></div>
                <div class="col-md-8" ><h2>Agora os headsets da HyperX tem certificação Discord</h2>
                <p> Os headsets HyperX Cloud II e Cloud Revolver ganharam a certificação Discord graças à clareza e nitidez da voz, além da excelência no som. </p>
                 
                 <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="location.href='http://www.hyperxgaming.com/br/cloud'">Conheça mais</button>
                </div></div>
                
                </div>

                <div class="col-md-12">
                  <h1>Headsets da HyperX são os primeiros a obter certificação Discord</h1>
                <p>HyperX Cloud II e o inédito Cloud Revolver são os primeiros dispositivos de áudio projetados para gamers a receber o certificado de qualidade do aplicativo de comunicação Discord.  </p>
                <p>A HyperX, divisão de produtos de alta performance da Kingston, acaba de anunciar que seu premiado headset Cloud II e o seu futuro lançamento Cloud Revolver são os primeiros dispositivos de áudio a obter a certificação de qualidade da Discord, aplicativo de chat de voz e texto para gamers. Apontado como o mais rápido da categoria, o <strong>Discord oferece serviço gratuito, seguro e de baixa latência para jogos multiplayer de desktop e mobile, </strong> possui cinco milhões de usuários ativos e tem mais de um milhão de pessoas experimentando seus serviços. </p>
                <p>Os headsets <strong>HyperX Cloud II e Cloud Revolver </strong> foram <strong>certificados pela Discord pela qualidade de voz clara e nítida, além de som excelente. Tudo isso </strong> após  uma rigorosa série de testes que não apontaram ecos, ruídos de fundo e distorções vocais. Os microfones dos dois headsets ainda passaram por comparações acústicas, exaustivos testes de gameplay com os cinco dos melhores jogos do mercado e avaliação de áudio da própria comunidade de usuários do aplicativo. </p>
                <p>A Discord trabalha com fabricantes de áudio do mais alto nível para garantir que os jogadores tenham a melhor experiência durante a comunicação com amigos, parceiros de time e colegas de jogo.“Tanto o HyperX Cloud Revolver quanto o Cloud II foram colocados à prova e passaram pelos testes com louvor”, explica Eros Resmini, CMO da Discord.  
                “É uma satisfação ser o primeiro fabricante de headsets gamers a obter a certificação Discord, que já é uma referência no mercado de comunicação de voz e texto dentro dos jogos”, disse Paulo Vizaco, diretor executivo da Kingston Brasil. <strong>“Ter essa certificação é mais uma prova do nosso compromisso com os jogadores e uma validação de que os headsets HyperX entregam aos usuários a melhor experiência de som. </strong>  
                  </p> 

                <div class="col-md-12 fondoRojo">
        
                <h3> HyperX é a linha do headsets que todo gamer precisa: </h3>
                <div class="col-md-6"> <p><strong> • Alta qualidade do som certificada</strong></p> </div>
                <div class="col-md-6"> <p><strong>• Garantia Kingston</strong></p></div>
               </div>

                </div>

                <div class="col-md-12 sinpadding">      
          <div class="col-md-6 sinpadding">
          <div class="tablaoscura2">
           <div class="col-md-12"><h1>HyperX Coud II </h1></div>
           <div class="col-md-4"> <img src="images/productos/cloud2.png"></div>

          <div class="col-md-8">
          <p> • Dispositivo de controle avançado de áudio USB com placa de som DSP <br>
            • Capacidade Hi-Fi com drivers de 53mm fornecendo qualidade suprema de áudio <br>
            • Som surround 7.1 virtual controlado por hardware <br>
            • Muito confortável, 100% em espuma memory foam no arco e nas almofadas de couro sintético <br>
            • Otimizado para jogadores profissionais com design fechado sobre a orelha para cancelamento passivo de ruído <br>
            • Compatível – Conectividade USB para PC & Mac. Compatível com Stereo com PS4, Xbox One1 e dispositivos móveis <br>
          </p>
          </div>
          <button class="btn btn-danger pull-right" onclick="location.href='http://www.hyperxgaming.com/br/cloud/hscp'" target="_blank">Conheça mais </button>     
          </div></div>

          <div class="col-md-6">
          <div class="tablaoscura2">
            <div class="col-md-12"><h1>HyperX Cloud Revolver <strong>Novo!</strong> </h1></div>
           <div class="col-md-4"> <img src="images/productos/revolver.png"></div>
          
           <div class="col-md-8">
          <p> • Palco de som com qualidade de estúdio <br>
          • Espuma inteligente exclusiva e couro sintético de alta qualidade <br>
          • Estrutura durável em aço <br>
          • Microfone destacável com anulação de ruído <br>
          • Drivers direcionais de 50 mm de próxima geração <br>
          • Compatibilidade com múltiplas plataformas
          </p>
          </div>
          <button class="btn btn-danger pull-right" onclick="location.href='http://www.hyperxgaming.com/br/cloud/hx-hscr'" target="_blank">Conheça mais</button>
          </div></div>
</div>

                
                
              


               </div>

   
             
 
            <div class="col-md-2" id="boxes">
            	 <article class="box-verde"><a target="_blank" href="http://www.hyperxgaming.com/br/cloud">
                  <p> Conheça a linha de headsets HyperX </p>
                  <img src="images/productos/cloud2.png">
                  </a>
                 </article>

              <article class="box-celeste">
                <a target="_blank" href="http://www.kingston.com/br/wheretobuy">
                   <p> Distribuidores Oficiais Kingston </p>  
                    <img src="images/sellokingston.png">
                 </a>

                 </article>
                 
                 
            </div>
            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
