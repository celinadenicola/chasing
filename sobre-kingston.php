<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>

    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <h1>Informações sobre Kingston</h1>
              <h2>Encontre aqui o que Kingston tem para você</h2>
            
              <div class="clearfix"></div>
              <p>&nbsp;</p>

                <div class="clearfix"></div>

            

                <!--<div class="col-md-12 fondo novidades-boxes3b">
                <div class="col-md-10">
                <h2>As novidades de HyperX na BGS 2015</h2>
                <p>HyperX lança novo headset Cloud Core na Brasil Gamer Show 2015 e demonstra a potência dos SSDs e Memórias voltado para gamers.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='chasing-better.php'">Conheça mais</button>
             
              </div>-->


                 <div class="col-md-12 fondo">
                <div class="col-md-10">
               <div class="col-md-3 padding-right"><img src="images/espaco-revendas.png" class="img-responsive"></div>
                <h2>O que é o Espaço Revendas?</h2>
                <p>A Kingston quer que você saiba ainda mais sobre a marca e seus produtos.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='espaco-revendas.php'">Conheça mais</button> </div>
              <div class="col-md-12 fondo">
                  <div class="col-md-10">
                      <div class="col-md-3 padding-right"><img src="images/cabeza-roja.png" class="img-responsive" width="120px"></div>
                <h2>Por que Kingston?</h2>
                <p>Conheça os benefícios que são uma oportunidade para incrementar as suas vendas.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='espaco-revendas.php#ancla-saiba'">Conheça mais</button> </div>
             

               </div>

   
             
 
            <div class="col-md-2" id="boxes">
            	 <article class="box-verde"><a target="_blank" href="http://www.espacorevendaskingston.com.br/ssd.php">
                  <p>SSD Kingston</p> 
                  <img src="images/ssdv300.png">
                    <p>Até 10 vezes mais rápido do que um HD.</p> 
                  </a>
                 </article>

              <article class="box-celeste">
                <a target="_blank" href="http://www.hyperxgaming.com/br/">
                   <p> Saiba mais sobre os produtos HyperX </p>  
                  <img src="images/Savage-CludHeadset.png" target="_blank">
                 </a>

                 </article>
                 <article class="box-rosa">
                  <a target="_blank" href="http://www.hyperxgaming.com/br/ssd/shss3">
                  <p><Conheça o HyperX SSD Savage. </p>
                  <img src="images/HyperX-Savage-SSD.png" target="_blank">
               
                    <p>Alto desempenho, mais capacidade.</p> 
                 </a>
                 </article>
                 
            </div>
            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
