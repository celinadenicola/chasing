 <!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
 
 <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="porquekingston">
    	<div class="container">
            <div class="col-md-10">
              <h1 style="margin-bottom:3px;">Obrigado</h1>
              <h2 style="margin-top:3px;">pelo seu interesse no Espaço Revendas Kingston.</h2>
              <p>Para fazer parte deste canal, preencha os campos abaixo e receba, por e-mail, comunicados, novidades e mais informações sobre Kingston e seus produtos.</p>
              <p>&nbsp;</p>
              <p>Campos obrigatórios(*)</p>
              </div>
              <form name="form1" method="post" action="registros/procesar_form.php">
              <div class="col-md-5">
              
                <div class="form-group">
                <p>Nome da empresa*</p>

                <input name="nomempresa" type="text" id="nomempresa" value="" class="form-control" placeholder="Exemplo Eletrônicos De Ponta" required>
                
                <p>Categoria da empresa*</p>
             
                      <select class="form-control" name="categoria" id="categoria" required>
                                 <option value="Grande Varejo ">Grande Varejo</option>
                                 <option value="Varejo Regional ">Varejo Regional</option>
                                 <option value="Etail">Etail</option>
                                  <option value="Revenda Corporativa">Revenda Corporativa</option>
                                  <option value="Papelaria ">Papelaria</option>
                                 <option value="Revenda Gamer ">Revenda Gamer </option>
                                 <option value="Revenda de Informática ">Revenda de Informática</option>
                                 <option value="Subdistribuidor ">Subdistribuidor</option>
                                 <option value="Importador">Importador</option>
                                 <option value="Integrador ">Integrador</option>
                                 <option value="Lojas de Telefonia ">Lojas de Telefonia</option>
                                 <option value="outros">Outros</option>
                      </select>
                
                <p>CNPJ</p>
                <input name="cnpj" type="text" id="cnpj" placeholder="Exemplo: 222" value="" class="form-control" >
                
                <p>Nome completo do responsável* </p>
                <input name="nomresponsable" type="text" id="nomresponsable" placeholder="Exemplo Jose Antônio Medeiros" value="" class="form-control" required>
                
                <p>Departamento/função* </p>
                <input name="funcion" type="text" id="funcion" placeholder="Exemplo Compras, Recursos humanos..." value="" class="form-control" required>
              
                <p>E-mail do responsável*</p>
                <input name="email" type="email" id="email" placeholder="Exemplo jose_santos@eletronicosdeponta.com.br" value="" class="form-control" required>
              
                <p>Telefone de contato* </p>
                <input name="telefono" type="text" id="telefono" placeholder="Exemplo 11 5647-5566" value="" class="form-control" required>
              
                <p>Endereço </p>
                <input name="domicilio" type="text" id="domicilio"  placeholder="Exemplo Rua dos Pinheiros" value="" class="form-control" >
              
                <p>CEP</p>
                <input name="cep" type="text" id="cep" placeholder="CEP" value="" class="form-control">
              
                <p>Cidade*</p>
                <input name="ciudad" type="text" id="ciudad" placeholder="Cidade" value="" class="form-control" required>
              
                <p>Estado </p>
                <input name="estado" type="text" id="estado" placeholder="Estado" value="" class="form-control">
              
                <p><strong>Filiais</strong></p>
                <label class="radio-inline"> 
                  <input type="radio" name="radio" id="radio" value="Sim">  Sim
                </label>     
                <label class="radio-inline"> 
                    <input type="radio" name="radio" id="radio" value="Não">Não
                </label>
              </div>
              
              </div>
              <div class="col-md-5">
              
                <div class="form-group">
                <p><strong>Compra produtos Kingston/HyperX de Distribuidor Oficial?</strong></p>
                 <label class="radio-inline"> 
                  <input type="radio" name="radio2" id="radio2" value="Si">  Sim
                </label>

                <label class="radio-inline"> 
                  <input type="radio" name="radio2" id="radio2" value="Não">  Não
                </label>
                 <label class="radio-inline"> 
                  <input type="radio" name="radio2" id="radio2" value="Não sei"> Não sei 
                </label>
                
            
                <p><strong>Produtos Kingston com maior interesse. Pode assinalar mais de uma opção</strong></p>
               
                <table border="0" width="100%">
                  <tr>
                    <td width="44%">&nbsp;</td>
                    <td width="17%">Kingston</td>
                    <td width="39%">HyperX</td>
                  </tr>
                  <tr>
                    <td>SSD<br></td>
                    <td><input type="checkbox" name="Kingston[]" id="k1" value="SSD Kingston"></td>
                    <td><input type="checkbox" name="Hyperx[]" id="h1" value="SSD HyperX"></td>
                  </tr>
                  <tr>
                    <td>Memórias <br></td>
                    <td><input type="checkbox" name="Kingston[]" id="k2" value="Memorias Kingston"></td>
                    <td><input type="checkbox" name="HyperX[]" id="h2" value="Memorias HyperX"></td>
                  </tr>
                  <tr>
                    <td>USB<br></td>
                      <td><input type="checkbox" name="Kingston[]" id="k3" value="USB Kingston"></td>
                    <td><input type="checkbox" name="HyperX[]" id="h3" value="USB HyperX"></td>
                  </tr>
                  <tr>
                    <td>Cartões de Memória<br></td>
                      <td><input type="checkbox" name="Kingston[]" id="k4" value="Cartões de Memoria Kingston"></td>
                    <td><input type="checkbox" name="HyperX[]" id="h4" value="Cartões de Memoria HyperX"></td>
                  </tr>
                  <tr>
                    <td>Wireless<br></td>
                   <td><input type="checkbox" name="Kingston[]" id="k5" value="Wireless Kingston"></td>
                    <td><input type="checkbox" name="HyperX[]" id="h5" value="Wireless HyperX"></td>
                  </tr>
                  <tr>
                    <td>Acessórios<br></td>
                   <td><input type="checkbox" name="Kingston[]" id="k6" value="Acessorios Kingston"></td>
                    <td><input type="checkbox" name="HyperX[]" id="h6" value="Acessorios HyperX"></td>
                  </tr>
                  <tr>
                    <td>Todos<br></td>
                    <td><input type="checkbox" name="Kingston[]" id="radioK" value="Seleccionó todos los productos Kingston" onclick="marcarTodosK()"></td>
                    <td><input type="checkbox" name="HyperX[]" id="radioH" value="Seleccionó todos los productos HyperX " onclick="marcarTodosH()"></td>
                  </tr>
                  <tr>
                    <td>Outros</td>
                    <td><input  type="text" name="otro" class="form-control"></input></td>
                  </tr>
                </table>
                
                <p><strong>Nome do seu Distribuidor Oficial </strong></p>
                
                <select class="form-control" name="nomdistribuidor" id="nomdistribuidor" onchange="select(this)">
                  <option value="Agis">Agis</option>
                  <option value="Alcateia">Alcateia</option>
                  <option value="Aldo">Aldo</option>
                  <option value="AllPlus">AllPlus</option>
                  <option value="ATC">ATC</option>
                  <option value="Handytech">Handytech</option>
                  <option value="Intcomex">Intcomex</option>
                  <option value="Mazer">Mazer</option>
                  <option value="Officer">Officer</option>
                  <option value="Pauta">Pauta</option>
                  <option value="SND">SND</option>
                  <option value="USTech">USTech</option>
                  <option value="Outros">Outros</option>
                </select>
                
                  <input name="otroDis" type="text" id="otroDis" placeholder="Outros Distribuidor" value=""
                 style='display:none;' class="form-control" >
                <p>Assunto que você tem mais interesse no Espaço de Revendas Kingston. </p>
                  <input type="checkbox" name="asuntos[]" id="informeprod" value="Informacion sobre productos">
                Informações sobre produtos<br>
                <input type="checkbox" name="asuntos[]" id="conocerdis" value="Conocer Distribuidores">
                Conhecer Distribuidores <br>
                <p>&nbsp;</p>
                 <h2 style="font-size:15px;">ESTOU DE ACORDO EM FORNECER MEUS DADOS PARA RECEBER E-MAILS DO ESPAÇO REVENDAS KINGSTON.</h2><br>


                  <label class="radio-inline"><input type="radio" name="acuerdo" id="radio4" value="Si">SIM</label>

                  <label class="radio-inline"><input type="radio" name="acuerdo" id="radio5" value="No">NÃO</label>

                <p>
                  <input type="submit" name="enviar" id="enviar" value="Enviar" class="btn btn-danger enviar-btn">
                </p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p><br>
                </p>
             </div>
              
              </div>
            </form>
            <!--BOXE LATERALES-->
          <div class="col-md-2" id="boxes">
            	<article class="colegas">
                	<p>Recomende o Espaço de Revendas para um colega</p>
</br></br>
                        <button class="btn btn-danger">Convide aqui</button></div></article>
                       
    	</div>
        

             <p>&nbsp;</p><p>&nbsp;</p>
             <!--CAJAS COLUMNAS-->
        <div class="container">
        <?php include("includes/bottom-box.php"); ?>
        
      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->
    <?php include("includes/footer.php"); ?>
      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
    <script type="text/javascript">
function marcarTodosK(){ 
    
     document.getElementById("k1").click();
      document.getElementById("k2").click();
       document.getElementById("k3").click();
        document.getElementById("k4").click();
         document.getElementById("k5").click();
          document.getElementById("k6").click();
      
} 

function marcarTodosH(){ 
   
     document.getElementById("h1").click();
      document.getElementById("h2").click();
       document.getElementById("h3").click();
        document.getElementById("h4").click();
         document.getElementById("h5").click();
          document.getElementById("h6").click();
} 



function select(thisTag){
          if (thisTag.value != "Outros"){ 
                    document.getElementById('otroDis').style.display = "none";
                    document.getElementById('otroDis').value = "";
                    
                  }else{ 
                    document.getElementById('otroDis').style.display = "";
                  }
          
                }
    </script>
</body>
</html>
