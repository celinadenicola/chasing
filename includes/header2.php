
<div class="col-md-12">
  <nav class="navbar navbar-default navbar-fixed-top">
      
      <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand page-scroll" href="index.php"><img src="images/kingston-blanco.png" alt="logo-Kingston"></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-default" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right nav-uno">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="index.php">HOME</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="ssd.php">SSD</a>
                    </li>
                    <li>
                        <a class="page-scroll" href=" http://www.kingston.com/br/memory/desktop-notebook" target="_blank">MEMÓRIAS</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.kingston.com/br/usb" target="_blank">USB</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.kingston.com/br/flash" target="_blank">CARTÕES DE MEMÓRIA</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="http://www.hyperxgaming.com/latam/cloud" target="_blank">ACESSÓRIOS</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="sobre-kingston.php">SOBRE KINGSTON</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="hyperx.php">HyperX</a>
                    </li>
                    <li>
                        <a class="page-scroll cadas" href="cadastro.php">Cadastre-se aqui</a>
                    </li>
                </ul>
            </div>
             </div>

    </nav></div>