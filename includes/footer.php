 <footer>
        <div class="container">

            <div class="col-md-8">
                <p>Siga a Kingston<br/>

               <p> <a href="https://www.facebook.com/KingstonBrasil" target="_blank" class="redes">
                    <i class="fa fa-facebook fa-2x" aria-hidden="true"></i></a>

                <a href="https://twitter.com/kingstonbrasil" target="_blank" class="redes">
                 <i class="fa fa-twitter fa-2x" aria-hidden="true"></i></a></a>

                <a href="https://www.youtube.com/user/KingstonTechnologyBR" target="_blank" class="redes">
                    <i class="fa fa-youtube-play fa-2x" aria-hidden="true"></i></a>

                    <a href="https://www.instagram.com/hyperxbrasil" target="_blank" class="redes">
                        <i class="fa fa-instagram fa-2x" aria-hidden="true"></i></a>
           </p> </div>
            

            <div class="col-md-4" style="text-align:right;">
                <a href="http://www.hyperxgaming.com/br/" target="_blank"><img src="images/hyperX.png" alt="logo-hiperX"><br/></a>
                <p>uma divisão da Kingston</p>
                 <p><a href="http://www.kingston.com/br/company/privacy">Política de Privacidade</a></p>
            </div>
            </div>
    </footer>

    	<div class="bg-footer">
          <p>©2015 Kingston Technology Corporation. Todos os direitos reservados.</p>
        </div>