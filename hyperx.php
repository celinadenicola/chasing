<!DOCTYPE html>
<html lang="br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>


</head>

<body id="page-top" class="index">

<!-- Navigation -->

<?php include("includes/header2.php"); ?>

<!--SECTION-->
<section class="chasing-better">
    <div class="container">
        <div class="col-md-10">

            <!--NOTA 1-->
            <div class="col-md-12 fondo-hyperx">
                <div class="col-md-3 hyperx1">
                    <img src="images/hyperx_banner.png">
                </div>
                <div class="col-md-8">
                    <h1>Produtos HyperX: Uma grande <br> oportunidade para o seu negócio </h1>
                    <h2>Com a expectativa de gerar 1 bilhão de reais no ano, o mercado de games se apresenta como um ótima oportunidade para sua revendas.</h2>
                    <h4>Brasil é hoje o maior mercado de games da América Latina:</h4>
                    <h5>
                        <li> Previsão de receita para 2016: R$1 bilhão;</li>
                        <li> Maior número de jogadores que gastam dinheiro com games do mundo: 61% dos 48.8 milhões de gamers que investem dinheiro ao redor do planeta; </li>
                        <li> 47.1 milhões de gamers jogam em PCs e 34.6 milhões em smartphones. </li>
                        Source: Newzoo Global Games Market report.
                    </h5>
                    <p> A HyperX, divisão de produtos de alta performance da Kingston, possui produtos para gamers, overclockers e entusiastas de tecnologia, e patrocina mais de 30 equipes de e-Sports em todo o mundo. No Brasil, promove jogos através de seus produtos como memórias, SSD, fones de ouvido de qualidade e teclados, o que traz novas oportunidades de crescimento para sua revenda.</p>
                    <p>&nbsp;</p>
                </div>
            </div>


            <!--NOTA 2
            <div class="col-md-12 fondo-hyperx">
            <div class="col-md-3 hyperx1">
            <img src="images/bgs.png">
            </div>
            <div class="col-md-8">
            <h1>HyperX será patrocinadora <br>de ouro na BGS 2016 </h1>
            <h2>De 1 à 5 de setembro no São Paulo Expo.</h2>
            <p>A BGS (Brasil Game Show) é o maior evento de games do Brasil. <br> E a HyperX promete agitar o público em um estande de 500m²  em sua quinta participação consecutiva nesta que é a maior feira de games da América Latina, com diversas atrações para jogadores de todos os estilos e plataformas. </p>
            <p>&nbsp;</p>
            </div>
            </div>-->


            <!--NOTA 3-->
            <div class="col-md-12 fondo-hyperx">
                <div class="col-md-7">
                    <h1>HyperX Portfolio para gamers </h1>
                    <h2>Amplo portfólio para seu o negócio.</h2>
                    <h4>O essencial para um verdadeiro gamer:</h4>
                </div>
                <div class="col-md-5">
                    <img src="images/tabla.png">
                </div>
                <table class="table" cellpadding="0" cellspacing="0" style="background-color:#e31837">
                    <tbody>
                    <tr class="tablenopadding">
                        <td width="20%"> <h2>Memórias</h2> <p>Alto desempenho para jogadores exigentes</p></td>
                        <td width="20%"><h2>SSD</h2> <p>Alta velocidade e performance de jogo</p></td>
                        <td width="20%"><h2>Dispositivos USB</h2> <p>Maior capacidade e rapidez</p></td>
                        <td width="20%"><h2>Headsets</h2> <p>Design para jogadores com alta qualidade do som</p></td>
                        <td width="20%"><h2>Periféricos</h2> <p>Qualidade e modernidade</p></td>

                    </tr>
                    <tr class="tablaoscura2">
                        <td><h4>Série FURY DDR3, DDR4</h4><img src="images/productos/ddr.png"></td>
                        <td><h4>FURY</h4><img src="images/productos/fury.png"></td>
                        <td></td>
                        <td><h4>DRONE</h4><img src="images/productos/headsets_drone.png"></td>
                        <td><h4>MOUSE PAD</h4><img src="images/productos/mousepad.png"></td>

                    </tr>
                    <tr class="tablaoscura1">
                        <td><h4>SAVAGE</h4><img src="images/productos/memorias_savage.png"></td>
                        <td><h4>SAVAGE</h4><img src="images/productos/ssd_savage.png"></td>
                        <td><h4>SAVAGE</h4><img src="images/productos/peri_savage.png"></td>
                        <td><h4>CLOUD I - CLOUD II</h4><img src="images/productos/headsets_cloud2.png"></td>
                        <td></td>

                    </tr>
                    <tr class="tablaoscura2">
                        <td><h4>Série PREDATOR</h4><img src="images/productos/memorias_predator.png"></td>
                        <td><h4>PREDATOR</h4><img src="images/productos/ssd_predator.png"></td>
                        <td><h4>PREDATOR</h4><img src="images/productos/peri_predator.png"></td>
                        <td><h4>REVOLVER</h4><img src="images/productos/headsets_revolver.png"></td>
                        <td></td>

                    </tr>

                    <tr class="tablaoscura1">
                        <td><h4>Série IMPACT </h4><img src="images/productos/memorias_impact.png"></td>
                        <td></td>
                        <td></td>
                        <td><h4>CLOUD X</h4><img src="images/productos/headsets_cloudx.png"></td>
                        <td></td>

                    </tr>

                    <tr>
                        <td align="center"><button class="btn btn-danger pull-center" onclick="window.location.href='http://www.hyperxgaming.com/br/memory'">SAIBA MAIS</button><a href="http://www.hyperxgaming.com/br/memory"></a></td>
                        <td align="center"><button class="btn btn-danger pull-center" onclick="window.location.href='http://www.hyperxgaming.com/br/ssd'">SAIBA MAIS</button><a href="http://www.hyperxgaming.com/br/ssd"></a></td>
                        <td align="center"><button class="btn btn-danger pull-center" onclick="window.location.href='http://www.hyperxgaming.com/br/usb'">SAIBA MAIS</button><a href="http://www.hyperxgaming.com/br/usb"></a></td>
                        <td align="center"><button class="btn btn-danger pull-center" onclick="window.location.href='http://www.hyperxgaming.com/br/headsets/cloud'">SAIBA MAIS</button><a href="http://www.hyperxgaming.com/br/headsets/cloud"></a></td>
                        <td align="center"><button class="btn btn-danger pull-center" onclick="window.location.href='http://www.hyperxgaming.com/br/mouse-pad/mpfp'">SAIBA MAIS</button><a href="http://www.hyperxgaming.com/br/mouse-pad/mpfp"></a></td>
                    </tr>
                    </tbody>
                </table>
            </div>
            </br></br>  </br></br>


        </div>



        <div class="col-md-2" id="boxes">

            <article class="box-celeste">
                <a target="_blank" href="http://espacorevendaskingston.com.br/certificacao.php">
                    <p>HyperX headset portfólio</p>
                    <img src="images/productos/headsets_cloud2.png" target="_blank">

                </a>
            </article>
            <article class="box-verde"><a target="_blank" href="https://youtu.be/DuBP0hi1kCM?t=1">
                    <p>HyperX Cloud Revolver </p>
                    <img src="images/youtubeGrande.png">

                </a>
            </article>
            <article class="box-rosa">
                <p>Espaço Revendas</p>
                <button class="btn btn-danger" onclick="window.location.href='cadastro.php'">Cadastre-se aqui</button>
                </a>
            </article>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>


        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <div class="clearfix">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p></div><!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

    </div>



    </div>
</section>

<div class="clearfix"></div>

<!--FOOTER-->

<?php include("includes/footer.php"); ?>

<!-- jQuery -->

<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/stellar.js"></script>
<script src="js/responsive-slider.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/validate.js"></script>
<script src="js/grid.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>
<script src="js/agency.js"></script>



<script src="js/wow.min.js"></script>
<script>
    wow = new WOW(
        {

        }   )
        .init();
</script>

<script>
    $('.bxslider').bxSlider({
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 170,
        slideMargin: 10
    });
</script>

<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } else {
            $('nav').removeClass('shrink');
        }
    })
</script>

</body>
</html>
