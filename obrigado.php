<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
 
 <?php include("includes/header.php"); ?>

   <!--SECTION-->
    <section class="porquekingston">
    	<div class="container">
            <div class="col-md-10">
              <h1 style="margin-bottom:3px;"></h1>
              <h2 style="margin-top:3px;">
               Sua informações foram enviadas.
                <br><br>
                Muito Brigado.</h2>
              <p></p>
              <p>&nbsp;</p>
              </div>
            
            <!--BOXE LATERALES-->
          <div class="col-md-2" id="boxes">
            	<p>Saiba Mais</p>
            	<article class="colegas">
                	<p>Recomende o Espaço de Revendas para um colega</p>
</br></br>
                        <button class="btn btn-danger">Convide aqui</button></div></article>
                       
    	</div>
        

             <p>&nbsp;</p><p>&nbsp;</p>
             <!--CAJAS COLUMNAS-->
        <div class="container">
        <?php include("includes/bottom-box.php"); ?>
        
      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->
    <?php include("includes/footer.php"); ?>
      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
