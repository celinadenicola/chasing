<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <!--<h4 class="destaque">Imperdível </h4> -->
              <h1><span class="h1chico">Novidades</span> Próximos lançamentos </h1>
     
             
                <div class="clearfix"></div>
            
             
             
            
       
       <div class="clearfix">
       
       </div>
        

             
    <!--TABLA-->   
    <div class="col-md-12 sinpadding">      
          <div class="col-md-6 sinpadding">
         <div class="tablaoscura2">
      <td>
        <div class="col-md-8"><h1>SSD E 1000</h1>
      <h2> + de 1 milhão de instrução por segundo com o E1000.</h2> </div>
      <div class="col-md-4"><img src="images/productos/Kingston-Logo.png"></div>
      <div class="clearfix"></div>
      <p> Esta é a capacidade do Kingston E1000, um SSD com conexão PCI Express capaz de alcançar altos patamares de desempenho sem sobrecarregar o processador.</p>
      <button class="btn btn-danger pull-right" onclick="location.href='https://www.youtube.com/watch?feature=player_embedded&v=WBXNROXDWSE'" target="_blank">Assista ao video</button><img align="right" src="images/youtube.png" width="34" height="34" alt=""/>
      </div></div>
        <div class="col-md-6">
          <div class="tablaoscura2">
          <div class="col-md-8">  <h1>HyperX Revolver </h1> 
      <h2>Novo Headset top de linha.</h2> </div>

      <div class="col-md-4"><img src="images/productos/HyperX-Logo.png"></div>
        <div class="clearfix"></div>
       <div class="col-md-4"><img src="images/productos/headset-revolver.png"></div>
         <div class="col-md-8"><p> Chegou o headset top de linha que faz combo com os óculos de realidade virtual HTC Vive e o joystick Vive Pre. Os 3 oferecem experiência completa de imersão em jogos. </p></div>
      <button class="btn btn-danger pull-right" onclick="location.href='https://www.youtube.com/watch?feature=player_embedded&v=Roq0eY9pODU'" target="_blank">Assista ao video</button><img align="right" src="images/youtube.png" width="34" height="34" alt=""/>
     </div></div>
</div>
<div class="col-md-12 sinpadding">      
          <div class="col-md-6 sinpadding">
          <div class="tablaoscura2">
        <h1>Cloud X </h1> 
      <h2>Um headset pensado para o Xbox One. </h2> 
      <p> O acessório chega com as mesmas cores do Controle Elite e vem, inclusive, com uma embalagem similar para transporte.</p>
      <button class="btn btn-danger pull-right" onclick="location.href='https://www.youtube.com/watch?feature=player_embedded&v=WBXNROXDWSE'" target="_blank">Assista ao video</button><img align="right" src="images/youtube.png" width="34" height="34" alt=""/>      
     </div></div>
<div class="col-md-6">
            <div class="tablaoscura2">
        <h1>HyperX Revolver S</h1> 
      <h2>Som Surround 7.1</h2> 
      <p> O headset top de linha que virá em duas versões: o headset com áudio estéreo HyperX Revolver e o modelo com som 7.1 surround. </p>
      <button class="btn btn-danger pull-right" onclick="location.href='https://www.youtube.com/watch?time_continue=1&v=b2RoLzeFEcI'" target="_blank">Assista ao video</button><img align="right" src="images/youtube.png" width="34" height="34" alt=""/>
      </div></div>
</div>
<div class="col-md-12 fondoRojo">
        
      <h3> 3a geração do MobileLite e o pendrive Data Traveler 2000.</h3>
      <div class="col-md-3"><img src="images/productos/MobileLite-dos.png"></div>
      <div class="col-md-9"> </div><p>Conheça o <strong> novo MobileLite</strong> , que é roteator wireless, carregador power bank, porta USB para compartilhamento de arquivo. E também o <strong> Data Traveler 2000</strong> , um pendrive criptografado, com teclado alfanumérico para senha.</p>
      </div>

 


            
          </div>
          

           <div class="col-md-2" id="boxes">
              <article class="box-rosa"><a target="_blank" href="https://www.youtube.com/user/KingstonHyperXBR">
                  <p>Visite o canal Hyperx Brasil no Youtube </p> 
                  <img src="images/youtubeGrande.png">
                 
                  </a>
                 </article>
               
                 <article class="box-celeste"><a target="_blank" href="http://www.kingston.com/br/ssd/v">
                  <h4>SSD Kingston</h4> 
                  <img src="images/ssdv300.png">
                    <p>até 10 vezes mais rápido</p> 
                  </a>
                 </article>
                 

            </div>
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

                </div>
                  
   
   
      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
