<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="porquekingston">
    	<div class="container">
            <div class="col-md-10">
              <h1>Espaço exclusivo para revendedores da Kingston</h1>
              <p>Presente no mercado há 28 anos, a <strong>Kingston acredita na importância da revenda</strong>, por isso, criou este Espaço com informações exclusivas.</p>
              <h3>Veja o que encontrar no Espaço Revendas Kingston</h3>  
              <p>&nbsp;</p> 
              <div class="col-md-5">
                <article class="center">
                <a href="http://cmc.kingston.com/assetbank-kingston/action/browseItems?categoryId=338&categoryTypeId=2"><img src="images/iconos/arquivos.png" width="108" height="108" alt="Arquivos" class="center"></a>
                <p class="rosa"><strong>Arquivos Kingston</strong></p>
                <p>Imagens de produtos, folhetos, vídeos, banners e etc.</p>
                </article>
                <article class="center">
                <a href="http://espacorevendaskingston.com.br/cadastro.php"><img src="images/iconos/cadastro.png" width="108" height="108" alt="Arquivos" class="center"></a>
                  <p class="verde"><strong>Cadastro para Espaço Revendas.</strong></p>
                  <p>Receba informações, notícias e convites e muito mais sobre este canal.</p>
                </article>
              </div>
              <div class="col-md-5">
              <article class="center">
                <p><span class="center">
                <a href="http://www.kingston.com/br/wheretobuy"><img src="images/iconos/rede.png" width="108" height="108" alt="Arquivos" class="center"></a>
                </span></p>
                <p class="azul"><strong>Rede de Distribuidores Oficiais</strong></p>
                <p>Conheça a rede de distribuidores oficiais da Kingston do Brasil.</p>
              </article>
                <article class="center">
                <a href="mailto:contato@espacorevendaskingston.com.br?subject=Espaço%20Revendas%20Kingston" target="_blank"><img src="images/iconos/contato.png" width="106" height="106" alt="Arquivos" class="center"></a>
                  <p class="celeste"><strong>Entre em contato</strong></p>
                  <p>Canal direto entre revendedor e Kingston para perguntas, dúvidas ou sugestões.</p>
                </article>
              </div>
              
            </div>

            <div class="col-md-2" id="boxes">
          
                 <article class="box-celeste"><a target="_blank" href="http://www.kingston.com/br/company/whychoosekingston">
                  <img src="images/sellokingston.png">
                  <h4>Por que Kingston?</h4> 
                  </a>
                 </article>
                 <article class="box-rosa">
                    <h2>Espaço Revendas</h2>
                    <button class="btn btn-danger" onclick="window.location.href='cadastro.php'">Cadastre-se aqui</button>
                 </article>
                 <div class="clearfix" id="ancla-saiba"></div>
               <p>&nbsp;</p>
               <p>&nbsp;</p>
               <p>&nbsp;</p>
            </div>

             <!--TABLA VOIÇE SAIBA--> 
            <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>         
          <div class="col-md-10 voce-saiba">
          <img class="globo"src="images/globo.png" alt="globo-pregunta">
          <h1>Você Saiba?<br/>
          <span>Benefícios Kingston no Brasil</span></h1>
                <h2>As revendas oficias da Kingston no Brasil contam com o apoio de:</h2>
                <div class="col-md-6">
                <ul>
                  <li><strong>Suporte técnico em português</strong></li>
                  <li><strong>Garantia</strong></li>
                  <li><strong>RMA Local</strong></li>
                </ul>
                 </div>
                <div class="col-md-6">
                <ul>
                <li><strong>Atendimento ao cliente</strong></li>
                  <li><strong>Configurador de memória</strong></li>
                  <li><strong>Mais de 18 anos no Brasil</strong></li>
                  </ul>
                  </div>
            </div>

            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
