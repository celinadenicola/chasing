<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <h1>Novidades e notícias da Kingston</h1>
            
              <div class="clearfix"></div>
              <p>&nbsp;</p>

                <div class="clearfix"></div>

                <div class="col-md-12 novidades-boxes5b fondo">
                <div class="col-md-10">
                <h2>Promoção Mega Viagem</h2>
                <p>Convide o seu cliente para comprar produtos Kingston e concorrer a uma viagem para Orlando, na Flórida, com 3 acompanhantes.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='novidades-hyperx.php'">Conheça mais</button>
             
              </div>
              
               <div class="col-md-12 novidades-boxes6 fondo">
                <div class="col-md-10">
                <h2>Kingston e HyperX lançam novos produtos na maior feira de tecnologia da América</h2>
                <p>Veja tudo o que foi apresentado e testado na CES 2016, em Las Vegas, e que em breve chegará ao Brasil.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='ces.php'">Conheça mais</button>
             
              </div>

                <div class="col-md-12 novidades-boxes3b fondo">
                <div class="col-md-10">
                <h2>As novidades de HyperX na BGS 2015</h2>
                <p>HyperX lança novo headset Cloud Core na Brasil Gamer Show 2015 e demonstra a potência dos SSDs e Memórias voltado para gamers.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='novidades-hyperx.php'">Conheça mais</button>
             
              </div>
                 <div class="col-md-12 novidades-boxes3 fondo">
                <div class="col-md-10">
                <h2>Tem novidade sobre a Kingston.</h2>
                <p>A nova campanha Chasing Better da Kingston reforça o seu amplo portfolio e aplicações, que vão muito além do pendrive.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='chasing-better.php'">Conheça mais</button>
             
              </div>

               </div>

   
             
 
            <div class="col-md-2" id="boxes">
            	 <article class="box-verde"><a target="_blank" href="http://www.espacorevendaskingston.com.br/ssd.php">
                  <p>SSD Kingston</p> 
                  <img src="images/ssdv300.png">
                    <p>Até 15 vezes mais rápido do que um HD.</p> 
                  </a>
                 </article>

              <article class="box-celeste">
                <a target="_blank" href="http://www.hyperxgaming.com/br/">
                   <p> Saiba mais sobre os produtos HyperX </p>  
                  <img src="images/Savage-CludHeadset.png" target="_blank">
                 </a>

                 </article>
                 <article class="box-rosa">
                  <a target="_blank" href="http://www.hyperxgaming.com/br/ssd/shss3">
                  <p><Conheça o HyperX SSD Savage. </p>
                  <img src="images/HyperX-Savage-SSD.png" target="_blank">
               
                    <p>Alto desempenho, mais capacidade.</p> 
                 </a>
                 </article>
                 
            </div>
            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
