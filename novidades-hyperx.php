<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>

    <!-- Bootstrap Core CSS -->
    
    <link href="css/bootstrap2.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/jquery.bxslider.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/styles2016.css">

    <!-- Custom Fonts -->
    <link href='https://fonts.googleapis.com/css?family=Oswald:400,300,700' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

     <!--GOOGLE ANALYTICS-->   
    <script>
 (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
 (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
 m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
 })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

 ga('create', 'UA-68649221-1', 'auto');
 ga('send', 'pageview');

</script>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <h1>HyperX headsets: conforto e som extremo</h1>
              
                <div class="col-md-10">
               
                <p>Os headsets da HyperX foram um dos sucessos na maior feira de games da America Latina. 
Eles foram desenvolvidos especialmente para atender o mercado de Games  que continua em constante crescimento. 
Além de serem os mais confortaveis do mercado , apresenta extrema qualidade sonora, os headsets irão  incrementar seus vendas. </p>
                </div>

                
                <div class="clearfix"></div>
            
             
             
            
       
       <div class="clearfix">
       
       </div>
        <div class="col-md-12">
        
          
          <h3 class="rojo">Veja a opinião da midia especializada:
</h3>
  
         <a href="http://www.hyperxgaming.com/br/Reviews?prodCat=headsets" target="_blank"><img src="images/hyperx_opiniones2.png" class="img-responsive" alt=""/>  </a></br></br>  </br></br>
         </div>

           <div class="row">
              <div class="col-md-10">
              <h3 class="rojo">Compare os headsets HyperX e veja porque são um grande negócio: </h3>
              </div> 
              </div>
              
              <div class="clearfix"></div>
             
    <!--TABLA-->         
          <div class="col-md-12">
          <table class="table" cellpadding="0" cellspacing="0" style="background-color:#7a8580">
  <tbody>
    <tr class="tablenopadding">
      <td width="33%"><img src="images/tittle-core.png" class="img-responsive" alt="NOVO HyperX Cloud Core"/></td>
      <td width="33%"><img src="images/tittle-cloud.png" class="img-responsive" alt="HyperX Cloud"/></td>
      <td width="33%"><img src="images/tittle-cloud2.png" class="img-responsive" alt="HyperX Cloud II"/></td>
    </tr>
    <tr class="tablaoscura1">
      <td>O  melhor custo-benefício, construído em alumínio com acabamento em preto. Indicado para gamers e para pessoas que querem muita qualidade ao ouvir música.
</td>
      <td>Ótima qualidade sonora e um conforto especial para que o jogador possa ficar horas jogando. </td>
      <td>Aplicação de som sem distorção e filtro digital de áudio, permitindo que apenas a voz do jogador seja ouvida pelos outros competidores.  </td>
    </tr>
    <tr class="tablaoscura2">
      <td>Principais acessórios: microfone removível e alto-falantes de 53mm.</td>
      <td>Principais acessórios: Traz conchas extras em veludo, adaptadores de avião, celulares, tablets e PS4.</td>
      <td>Principais acessórios: Placa de som amplificadora USB, com controle de volume e interruptor de surround 7.1</td>
    </tr>
    <tr class="tablaoscura1">
      <td>Cor preto e vermelho</td>
      <td>Cores preto e vermelho, preto e branco</td>
      <td>Cores vermelho metálico, cinza metálico, pink e branco. </td>
    </tr>
    <tr>
      <td align="right"><button class="btn btn-danger pull-right" onclick="window.location.href='https://www.youtube.com/watch?v=-EnvUm8PtzI'">VER MAIS</button><a href="https://www.youtube.com/watch?v=31KC87Wb8WI"><img src="images/youtube.png" width="34" height="34" alt=""/></a></td>
      <td align="right"><button class="btn btn-danger pull-right" onclick="window.location.href='https://www.youtube.com/watch?v=I9tIYXhij24'">VER MAIS</button><a href="https://www.youtube.com/watch?v=I9tIYXhij24"><img src="images/youtube.png" width="34" height="34" alt=""/></a></td>
      <td align="right"><button class="btn btn-danger pull-right" onclick="window.location.href='http://https://www.youtube.com/watch?time_continue=1&v=b2RoLzeFEcI'">VER MAIS</button><a href="https://www.youtube.com/watch?v=b2RoLzeFEcI"><img src="images/youtube.png" width="34" height="34" alt=""/></a></td>
    </tr>
  </tbody>
</table></div>

</br></br>  </br></br>

            
          </div>
          

           <div class="col-md-2" id="boxes">
            	
              <article class="box-celeste">
                <a target="_blank" href="http://www.hyperxgaming.com/br/">
                   <p>Saiba mais sobre os produtos HyperX</p>  
                  <img src="images/Savage-CludHeadset.png" target="_blank">
                
                 </a>
                 </article>
                  <article class="box-verde"><a target="_blank" href="https://www.youtube.com/user/KingstonHyperXBR">
                  <p>Visite o canal Hyperx Brasil no Youtube </p> 
                  <img src="images/youtubeGrande.png">
                 
                  </a>
                 </article>
                 <article class="box-rosa">
                  <p>Espaço Revendas</p>
                	<button class="btn btn-danger" onclick="window.location.href='cadastro.php'">Cadastre-se aqui</button>
                  </a>
              </article>
                     <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
            
                 
            </div>
             <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
            
         <div class="clearfix">
         <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
           <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
           <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p></div><!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

                </div>
                  
   
   
      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
