<!DOCTYPE html>
<html lang="br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

<!-- Navigation -->

<?php include("includes/header2.php"); ?>

<!--SECTION-->
<section class="chasing-better">
    <div class="container">
        <div class="col-md-10">

            <!--NOTA 1-->
            <div class="col-md-12 fondo-hyperx">
                <div class="col-md-6">
                <h1>Primeiro teclado gamer da HyperX no Brasil</h1>
                <h2>HyperX Alloy FPS marca a entrada no segmento de teclados para gamers que curtem jogos de tiro em primeira pessoa (FPS).</h2>
                <p>A HyperX, divisão da Kingston focada em produtos de alta performance para gamers, overclockers, casemodders e entusiastas de tecnologia, acaba de anunciar seu primeiro teclado, o Alloy FPS e o mouse pad para video jogos FURY Pro.
                </p>
                </div>
                <div class="col-md-6 hyperx1">
   <a href="http://www.hyperxgaming.com/br/keyboards/HX-KB1" target="_blank"><img src="images/alloy.jpg"></a>

                </div>
            </div>


            <!--NOTA 2
            <div class="col-md-12 fondo-hyperx">
            <div class="col-md-3 hyperx1">
            <img src="images/bgs.png">
            </div>
            <div class="col-md-8">
            <h1>HyperX será patrocinadora <br>de ouro na BGS 2016 </h1>
            <h2>De 1 à 5 de setembro no São Paulo Expo.</h2>
            <p>A BGS (Brasil Game Show) é o maior evento de games do Brasil. <br> E a HyperX promete agitar o público em um estande de 500m²  em sua quinta participação consecutiva nesta que é a maior feira de games da América Latina, com diversas atrações para jogadores de todos os estilos e plataformas. </p>
            <p>&nbsp;</p>
            </div>
            </div>-->


            <!--NOTA 3-->
            <div class="col-md-12 fondo-hyperx detalles">
                <div class="col-md-12">
                    <h2>Incremente o seu portfólio para gamers</h2>
                </div>

                <div class="col-md-3">
                    <img src="images/1.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/2.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/3.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/4.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/5.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/6.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/7.jpg">
                </div>
                <div class="col-md-3">
                    <img src="images/8.jpg">
                </div>

            </div>
            </br></br>  </br></br>

            <div class="col-md-12 fondo-hyperx detalles2">
                <div class="col-md-12">
                    <h2> Linha completa de produtos para gamers e entusiastas</h2>
                </div>
                <div class="col-md-4">
                    <div class="hovereffect">
                        <img src="images/9.jpg">
                            <div class="overlay">

                                <p>
                                    <a href="http://www.hyperxgaming.com/br/headsets/stinger/hx-hscs">HyperX Stinger</a>
                                </p>
                            </div>
                    </div>

                   <a href="http://www.hyperxgaming.com/br/headsets/stinger/hx-hscs"> <h4><span>HyperX Stinger headset: </span> mais qualidade de som e praticidade</h4></a>
                </div>
                <div class="col-md-4">
                    <div class="hovereffect">
                        <img src="images/10.jpg">
                        <div class="overlay">
                            <p>
                                <a href="http://www.hyperxgaming.com/br/mouse-pad/mpfp" target="_blank">Mouse Pad</a>
                            </p>
                        </div>
                    </div>


                    <a href="http://www.hyperxgaming.com/br/mouse-pad/mpfp" target="_blank"> <h4><span>Mouse Pad </span> FURY Pro Gaming</h4></a>
                </div>
                <div class="col-md-4">
                    <div class="hovereffect">
                        <img src="images/11.jpg">
                        <div class="overlay">

                            <p>
                                <a href="http://www.hyperxgaming.com/br/ssd/shfs37a" target="_blank">FURY SSD</a>
                            </p>
                        </div>
                    </div>

                    <a href="http://www.hyperxgaming.com/br/ssd/shfs37a"> <h4><span>Computador </span> com alto desempenho </h4></a>
                </div>

            </div>


        </div>



        <div class="col-md-2" id="boxes">
            <article class="box-celeste">
                <a target="_blank" href="https://youtu.be/tmvU7YXaAqw">
                    <img src="images/banners/stinger.jpg" class="img-responsive">

                </a>
            </article>
            <article class="box-verde">
                <a target="_blank" href="https://youtu.be/D1PhReeWHeQ">
                    <img src="images/banners/alloy.jpg" class="img-responsive">
                </a>
            </article>
            <article class="box-azul">
                <a target="_blank" href="https://youtu.be/anfk4RO58_k">
                 <img src="images/banners/pad.jpg" class="img-responsive">
                </a>
            </article>

        </div>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <p>&nbsp;</p>

        <div class="clearfix">
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p></div><!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

    </div>



    </div>
</section>

<div class="clearfix"></div>

<!--FOOTER-->

<?php include("includes/footer.php"); ?>

<!-- jQuery -->

<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/stellar.js"></script>
<script src="js/responsive-slider.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/validate.js"></script>
<script src="js/grid.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>
<script src="js/agency.js"></script>



<script src="js/wow.min.js"></script>
<script>
    wow = new WOW(
        {

        }   )
        .init();
</script>

<script>
    $('.bxslider').bxSlider({
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 170,
        slideMargin: 10
    });
</script>

<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } else {
            $('nav').removeClass('shrink');
        }
    })
</script>

</body>
</html>
