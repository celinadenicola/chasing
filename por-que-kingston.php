<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>

    <?php include("inc/head.php"); ?>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header.php"); ?>

   <!--SECTION-->
    <section class="porquekingston">
    	<div class="container">
            <div class="col-md-10">
              <h1>Por qué Kingston?</h1>
              <h2>Espaço exclusivo para revendedores da marca.</h2>
              <p>Presente no mercado há 28 anos, a Kingston acredita na importância da revenda para a expansão da marca e, por isso, quer ajudar os seus revendedores a entenderem cada vez mais sobre os seus produtos.</p>
              <p>E para garantir que tanto quem vende, quanto quem compra tenha todas as respostas, a Kingston criou o Espaço de Revendas. Aqui, os revendedores tem informação sobre produtos, soluções, usos e recomendações, além de ferramentas de apoio para o processo de vendas. </p>
              <h3>Veja o que encontrar no Espaço de Revendas</h3>
              <div class="col-md-5">
                <article class="center"><img src="images/iconos/arquivos.png" width="108" height="108" alt="Arquivos" class="center">
                <p class="rosa"><strong>Arquivos Kingston</strong></p>
                <p>Cartazes, imagens de produtos, folhetos, vídeos, banners e etc.</p>
                </article>
                <article class="center"><img src="images/iconos/cadastro.png" width="108" height="108" alt="Arquivos" class="center">
                  <p class="verde"><strong>Cadastro para ser membro do Espaço de Revendas</strong></p>
                  <p>Faça seu cadastro ou atualize os seus dados para receber informações, notícias, convites e muito mais sobre este canal. mensuais, informações.</p>
                </article>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
                <p>&nbsp;</p>
              </div>
              <div class="col-md-5">
              <article class="center">
                <p><span class="center"><img src="images/iconos/rede.png" width="108" height="108" alt="Arquivos" class="center">
                </span></p>
                <p class="azul"><strong>Rede de Distribuidores Oficiais</strong></p>
                <p>Lista com a ampla rede de distribuidores da marca Kingston no Brasil.</p>
              </article>
                <article class="center"><img src="images/iconos/contato.png" width="106" height="106" alt="Arquivos" class="center">
                  <p class="celeste"><strong>Entre em contato</strong></p>
                  <p>Canal direto entre revendedor e Kingston para perguntas, dúvidas ou sugestões.</p>
                </article>
              </div>
            </div>
            <div class="col-md-2" id="boxes">
            	<p>Saiba Mais</p>
            	<article class="box-verde">
                	<p>Seja parte do</p>
                    <h2> Kingston</h2>
                 </article>
                 <article class="box-rosa">
                	<p>SSD da Kingston<br>
               	    podem substituir HD</p>
                    <h2>Descubra <br>
                    o porquê</h2>
              </article>
                 <article class="box-celeste">
                	<p>Seja parte do</p>
                    <h2>Espaço Kingston</h2>
                 </article>

            </div>

             <!--TABLA VOIÇE SAIBA-->
            
          <div class="col-md-10 voce-saiba">
          <img class="globo"src="images/globo.png" alt="globo-pregunta">
          <h1>Você Saiba?<br/>
          <span>Beneficios Kingston no Brasil</span></h1>
                <h2>As revendas oficias da Kingston no Brasil contam com o apoio de:</h2>
                <div class="col-md-4">
                  <img src="images/produto-oficial.png" alt="">
                </div>
                <div class="col-md-8">
                <ul>
                  <li>Suporte técnico em português</li>
                  <li>Garantia</li>
                  <li>RMA Local</li>
                  <li>Atendimento ao cliente</li>
                  <li>Configurador de memória</li>
                </ul>
                </div>
            </div>

            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASm3CwaK9qtcZEWYa-iQwHaGi3gcosAJc&sensor=false"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/jquery.nicescroll.min.js"></script>
    <script src="js/fancybox/jquery.fancybox.pack.js"></script>
    <script src="js/jquery.parallax-1.1.3.js" type="text/javascript" ></script>
    <script src="js/skrollr.min.js"></script>       
    <script src="js/jquery.scrollTo-1.4.3.1-min.js"></script>
    <script src="js/jquery.localscroll-1.2.7-min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/main.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   
        <script type="text/javascript">
            // When the window has finished loading create our google map below
            google.maps.event.addDomListener(window, 'load', init);
        
            function init() {
                // Basic options for a simple Google Map
                // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
                var mapOptions = {
                    // How zoomed in you want the map to start at (always required)
                    zoom: 11,

                    // The latitude and longitude to center the map (always required)
                    center: new google.maps.LatLng(40.6700, -73.9400), // New York

                    // How you would like to style the map. 
                    // This is where you would paste any style found on Snazzy Maps.
                    styles: [   {       featureType:"all",      elementType:"all",      stylers:[       {           invert_lightness:true       },      {           saturation:10       },      {           lightness:30        },      {           gamma:0.5       },      {           hue:"#1C705B"       }       ]   }   ]
                };

                // Get the HTML DOM element that will contain your map 
                // We are using a div with id="map" seen below in the <body>
                var mapElement = document.getElementById('map');

                // Create the Google Map using out element and options defined above
                var map = new google.maps.Map(mapElement, mapOptions);
            }
        </script>
         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
