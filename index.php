<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="ESPAÇO REVENDAS Kingston">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    
<?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="principal">
        <div class="container">
            <div class="col-md-3 col-cadastre"><a href="http://www.kingston.com/br/ssd/consumer/suv400s3">
            	<img src="images/ssdnow.png" alt="cadastre" class="img-rounded">
            </div>

            <div class="col-md-9 col-slide">
     			
     			<!--SLIDE-->

       			<div id="carousel-a" class="carousel slide carousel-sync slider-home" data-ride="carousel" data-pause="false">
				  <div class="carousel-inner">


                      <!--1 ALLOY-->
                      <div class="item active seis">
                          <a href="alloy.php">

                              <img src="images/slide/Alloy.png" alt="slide0">
                          </a>
                      </div>
                      <!--3 STINGER-->
                      <div class="item rojo">
                          <a href="http://www.hyperxgaming.com/br/headsets/stinger/hx-h" target="_blank">

                              <img src="images/slide/Stinger.png" alt="slide0">
                          </a>
                      </div>

                      <!--3 HYPERX REVOLVER-->
                      <div class="item cuatro">
                          <a href="lancamentos.php">
                              <div class="col-md-6">
                                  <h1>HyperX Headset Cloud Revolver</h1>
                                  <h3>Palco sonoro, drives, compatibilidade, para os gamers mais exigentes.</h3>

                              </div>
                              <img src="images/slide/05.png" alt="slide0">
                          </a>
                      </div>

                      <!--4 ESPACO REVENDAS-->

              <div class="item dos">
                  <a href="espaco-revendas.php">
              <div class="col-md-6">
              <h1>Espaço Revendas</h1>
                         <h3>Um canal da Kingston, com informações e ferramentas para melhorar as suas vendas.</h3>
                       
              </div>
                <img src="images/slide/08.png" alt="slide0">
                </a>  
              </div>


                      <!--5 hyperx-->
             <div class="item seis"><a href="hyperx.php">
				    	<div class="col-md-6">
				    	<h1>Entre no jogo <br>com HyperX</h1>
                         <h3>O mercado do games espera gerar 1 bilhão de reais em 2016. A oportunidade perfeita para o seu negócio vender produtos HyperX.</h3>
                        
				      </div>
				      	<img src="images/slide/06.png" alt="slide3">
                </a>
				      </div>
                      <!--6 ESPACO REVENDAS-->
                                  <div class="item siete"><a href="#">
                                  <div class="col-md-6">
                                  <h1>Os melhores upgrades de computador com as novas SSD da Kingston
                    </h1>
                                             <h3>SSDNow UV 400 e V300 foram criadas para o mais alto desempenho, com um ótimo custo-benefício.</h3>

                                  </div>
                                    <img src="images/slide/07.png" alt="slide3">
                                    </a>
                                  </div>
                      <!--FIN ITEMS DE SLIDE-->
				  </div>
				</div>
            </div>

             <!--CLEAR-->
        <div class="clearfix"></div>

            <div class="destacado01">
            <h3></h3>
        </div>

         <!--CLEAR-->
        <div class="clearfix"></div>

        <!--CAROUSEL-->
        <div class="carruselProductos">
            <ul class="bxslider">
              <li><a href="http://www.kingston.com/br/memory" target="blank"><img src="images/ram.png"/>Memórias</a></li>
              <li><a href="http://www.kingston.com/br/ssd" target="blank"><img src="images/ssd.png"/>SSD</a></li>
              <li><a href="http://www.kingston.com/br/flash" target="blank"><img src="images/sd.png"/>Cartaõs de memória</a></li>
              <li><a href="http://www.kingston.com/br/usb" target="blank"><img src="images/pen.png"/>USB</a></li>
              <li><a href="http://www.hyperxgaming.com/br/cloud" target="blank"><img src="images/headset.png"/>Acessórios</a></li>
              <li><a href="http://www.hyperxgaming.com/br/ssd/shss3" target="blank"><img src="images/ssdSavage.png"/>SSD Savage</a></li>
              <li><a href="http://www.hyperxgaming.com/br/memory/fury-ddr4" target="blank"><img src="images/hyperXFury.png"/>Memória HyperX Fury</a></li>
              <li><a href="http://www.hyperxgaming.com/br/ssd/shfs37a" target="blank"><img src="images/ssdFury.png"/>SSD Fury</a></li>
            </ul>
        </div>  

        <!--CLEAR-->
        <div class="clearfix"></div>
    
        <!--CAJAS COLUMNAS-->
        
<?php include("includes/bottom-box.php"); ?>

        </div>
    </section>

<!--FOOTER-->
   
<?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 250,
  slideMargin: 0
});
    </script>

    <script>
	    $('.carousel-sync').on('slide.bs.carousel', function(ev) {
	  	var dir = ev.direction == 'right' ? 'prev' : 'next';
		$('.carousel-sync').not('.sliding').addClass('sliding').carousel(dir);
	});
	$('.carousel-sync').on('slid.bs.carousel', function(ev) {
		$('.carousel-sync').removeClass('sliding');
	});
	</script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
