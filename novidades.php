<!DOCTYPE html>
<html lang="br">
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>
</head>

<body id="page-top" class="index">

<!-- Navigation -->

<?php include("includes/header2.php"); ?>

<!--SECTION-->
<section class="chasing-better">
    <div class="container">
        <div class="col-md-10">
            <h1>Novidades e notícias da Kingston</h1>

            <div class="clearfix"></div>
            <p>&nbsp;</p>

            <div class="clearfix"></div>



            <!--<div class="col-md-12 fondo novidades-boxes3b">
            <div class="col-md-10">
            <h2>As novidades de HyperX na BGS 2015</h2>
            <p>HyperX lança novo headset Cloud Core na Brasil Gamer Show 2015 e demonstra a potência dos SSDs e Memórias voltado para gamers.</p>
            </div>
            <div class="clearfix"></div>
            <button class="btn btn-danger pull-right" onclick="window.location.href='chasing-better.php'">Conheça mais</button>

          </div>-->


            <!--noticia 2
              <div class="col-md-12 fondo">
              <div class="col-md-10">
             <div class="col-md-3 padding-right"><img src="images/productos/ssd-V400.png" class="img-responsive"></div>
              <h2>Troca Turbinada</h2>
              <p>A promoção da Aldo oferece R$ 20,00 de bônus na troca de um HDD antigo por um SSDNow UV400.
</p>
              </div>
              <div class="clearfix"></div>
              <button class="btn btn-danger pull-right" onclick="window.location.href='http://www.aldo.com.br/Portais/HotSites/Campanha.aspx?hsid=205&groupBy=mc'">Conheça mais</button> </div>-->



            <!--noticia 1-->
            <div class="col-md-12 fondo">
                <div class="col-md-10">
                    <div class="col-md-3 padding-right"><img src="images/productos/alloy.png" class="img-responsive"></div>
                    <h2>Teclado gamer da HyperX no Brasil</h2>
                    <p>HyperX Alloy FPS marca a entrada no segmento de teclados para gamers que curtem jogos de tiro em primeira pessoa (FPS).</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='ssd.php'">Conheça mais</button> </div>

            <!--noticia 2-->
            <div class="col-md-12 fondo">
                <div class="col-md-10">
                    <div class="col-md-3 padding-right"><img src="images/productos/stinger.png" class="img-responsive"></div>
                    <h2>HyperX Stinger headset: Maior qualidade de som e practicidade
                    </h2>
                    <p>Uma novidade para os gamers e entusiastas que procuram conforleveza y conforto, qualidade de som superior e maior practicidade.
                    </p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='ssd.php'">Conheça mais</button> </div>


            <!--noticia 3-->
            <div class="col-md-12 fondo">
                <div class="col-md-10">
                    <div class="col-md-3 padding-right"><img src="images/productos/ssd-V300.png" class="img-responsive"></div>
                    <h2>SSD da Kingston</h2>
                    <p>A melhor opção de upgrade para seus clientes.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='ssd.php'">Conheça mais</button> </div>

            <!--noticia 4-->
            <div class="col-md-12 fondo novidades-boxes3">
                <div class="col-md-10">
                    <div class="col-md-3 padding-right"><img src="images/productos/headsets.png" class="img-responsive"></div>
                    <h2>Headsets HyperX Cloud II e Cloud Revolver com certificado de qualidade</h2>
                    <p>HyperX Cloud II e o inédito Cloud Revolver são os primeiros dispositivos de áudio certificados no mundo.</p>
                </div>
                <div class="clearfix"></div>
                <button class="btn btn-danger pull-right" onclick="window.location.href='certificacao.php'">Conheça mais</button> </div>

        </div>




        <div class="col-md-2" id="boxes">
            <article class="box-verde"><a target="_blank" href="http://www.espacorevendaskingston.com.br/ssd.php">
                    <p>SSD Kingston</p>
                    <img src="images/ssdv300.png">
                    <p>Até 15 vezes mais rápido do que um HD.</p>
                </a>
            </article>

            <article class="box-celeste">
                <a target="_blank" href="http://www.hyperxgaming.com/br/">
                    <p> Saiba mais sobre os produtos HyperX </p>
                    <img src="images/Savage-CludHeadset.png" target="_blank">
                </a>

            </article>
            <article class="box-rosa">
                <a target="_blank" href="http://www.hyperxgaming.com/br/ssd/shss3">
                    <p><Conheça o HyperX SSD Savage. </p>
                    <img src="images/HyperX-Savage-SSD.png" target="_blank">

                    <p>Alto desempenho, mais capacidade.</p>
                </a>
            </article>

        </div>

        <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

    </div>
</section>

<div class="clearfix"></div>

<!--FOOTER-->

<?php include("includes/footer.php"); ?>

<!-- jQuery -->

<script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
<script src="js/jquery.js"></script>
<script src="js/jquery.easing.1.3.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.bxslider.js"></script>
<script src="js/jquery.isotope.min.js"></script>
<script src="js/stellar.js"></script>
<script src="js/responsive-slider.js"></script>
<script src="js/jquery.appear.js"></script>
<script src="js/validate.js"></script>
<script src="js/grid.js"></script>
<script src="js/classie.js"></script>
<script src="js/cbpAnimatedHeader.js"></script>
<script src="js/agency.js"></script>



<script src="js/wow.min.js"></script>
<script>
    wow = new WOW(
        {

        }   )
        .init();
</script>

<script>
    $('.bxslider').bxSlider({
        minSlides: 3,
        maxSlides: 5,
        slideWidth: 170,
        slideMargin: 10
    });
</script>

<script>
    $(window).scroll(function() {
        if ($(document).scrollTop() > 50) {
            $('nav').addClass('shrink');
        } else {
            $('nav').removeClass('shrink');
        }
    })
</script>

</body>
</html>
