<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <!--<h4 class="destaque">Imperdível </h4> -->
              <h1>Imperdível promoção de volta às aulas da Kingston </h1>
              

               <div class="col-md-12 novidades-boxes5">
                <div class="col-md-10">
                <h2>Promoção Mega Viagem </h2>
                <p>Aproveite o período de volta às aulas e aumente suas vendas de pendrive, cartão SD <br> e microSD Kingston.
Seu cliente concorre a uma viagem para Orlando, na Flórida, <br> com mais 3 acompanhantes. A promoção vai até 15 de março. </p>
                </div></div>
                <div class="clearfix"></div>
            
             
             
            
       
       <div class="clearfix">
       
       </div>
        

             
    <!--TABLA-->         
          <div class="col-md-12">
          <table class="table" cellpadding="0" cellspacing="0" style="background-color:rgba(234, 243, 242, 0.92)">
  <tbody>
    <tr class="tablenopadding">
      <td width="25%"></td>
      <td width="25%"></td>
      <td width="25%"></td>
      <td width="25%"></td>

    </tr>
    <tr class="tablaoscura1">
      <td></td>
      <td><h2>Cartões Flash SD </h2> </td>
      <td><h2>Cartões Micro SD </h2></td>
      <td><h2>Pendrives </h2></td>

    </tr>
    <tr class="tablaoscura4">
      <td><h2>Modelo Principal</h2></td>
      <td align="center"><img src="images/cartoes_flash.png"></td>
      <td align="center"> <img src="images/cartoes_microsd.png"> </td>
      <td align="center"><img src="images/pendrives.png"></td>

    </tr>
    <tr class="tablaoscura1">
      <td></td>
      <td>Classe 10 UHS-I</td>
      <td>Kit de Mobilidade Classe 10 (cartão micro SD + adaptador SD + adaptador USB) </td>
      <td>Data Traveler SE9 </td>

    </tr>
    <tr class="tablaoscura4">
      <td></td>
      <td> 
        - Taxa mínima de transferência de dados: 10MB <br>
        - Grava vídeo em full HD <br>
        - Grava vídeo em 3D <br>
        - Faz fotos sequenciais/ em ação <br>
        - Capacidade: <br>
        - 16GB/32GB/64GB/128GB/256GB/512GB <br>
      </td>
      <td> 
        - Pode ser usado em smartphones, câmeras e computador <br>
        - Taxa mínima de transferência de dados: 10MB <br>
        - Grava vídeo em full HD <br>
        - Grava vídeo em 3D <br>
        - Faz fotos sequenciais/ em ação  <br>
        - Capacidade: <br>
        4GB/8GB/16GB/32GB/64GB <br>
      </td>
      <td> 
        - Estrutura de metal
        - Argola grande
        - Formato reduzido
        - Entrada USB
        - Compatível com Windows® de 10 a 7, Windows Vista®, Mac OS X v.10.8.x ou superior, Linux v.2.6.x ou superior e OS™ 
      </td>

    </tr>
     <tr class="tablaoscura1">
      <td><h2>Benefícios</h2></td>
      <td> 
        - Garantia Vitalícia <br>
        - Suporte técnico gratuito <br>
        - Produto de alta qualidade <br>
        - 100% testado
      </td>
      <td> 
        - Garantia Vitalícia <br>
        - Suporte técnico gratuito <br>
        - Produto de alta qualidade <br>
        - 100% testado
      </td>
      <td>
        - Garantia de 5 anos <br>
        - Suporte técnico gratuito <br>
        - Produto de alta qualidade <br>
        - 100% testado
      </td>

    </tr>
    
    <tr class="tablaoscura4">
      <td><h2>Outros modelos que participam <br> da promoção</h2></td>
      <td align="left"><button class="btn btn-danger pull-left" >Mais modelos aqui</button><a href="http://www.kingston.com/br/flash/sd_cards"></a></td>
      <td align="left"><button class="btn btn-danger pull-left" >Mais modelos aqui</button><a href="http://www.kingston.com/br/flash/microsd_cards"></a></td>
      <td align="left"><button class="btn btn-danger pull-left" >Mais modelos aqui</button><a href="http://www.kingston.com/br/usb/personal_business#dtse9h"></a></td>
    </tr>
  </tbody>
</table></div>

   <div class="row">
    <div class="col-md-12">
          <h2>PASSO-A-PASSO </h2>
          <p>
            Como o cliente participa da Promoção Mega Viagem?
          </p>
        <a target="_blank" href="http://megaviagemkingston.com.br"> <img src="images/pasos.png"> </a>
        <h2>Prêmio: uma viagem para Orlando na Flórida com tudo pago para o ganhador e mais 3 acompanhantes. </h2> 

          </div>
              </div>
</br></br>  </br></br>

            
          </div>
          

           <div class="col-md-2" id="boxes">
              <article class="box-verde"><a target="_blank" href="http://media.kingston.com/pdfs/MKF_283.1_Flash_Memory_Guide_BR.pdf">
                  
                  <h4>Guia de memória flash</h4> 
                    <p>>> Descargue</p>
                  </a>
                 </article>
                 <article class="box-rosa"> <a target="_blank" href="http://www.kingston.com/br/usb/usb_30">
                  <img src="images/usb30_choose.png" width="110px">
                  <h4>O que é USB 3.1 Ger 1 USB 3.0?</h4>
                  <p></p>
                  </a>
              </article>
                 <article class="box-celeste"> <a target="_blank" href="http://espacorevendaskingston.com.br/cadastro.php">
                  <img src="images/espaco-revendas-logo.png" width="110px">
                  <h4>Cadastro Espaço Revendas </h4>
                  </a>
                 </article>
                 <article class="box-azul"> <a href="http://espacorevendaskingston.com.br/arquivos/17616-004-000_WOBBLER_15cm.pdf" target="_blank">
                  <img src="images/documento.png">
                  <h4>Baixe aqui o poster da promoção Mega Viagem e divulgue na sua Revenda.</h4>
                 
                  </a>
              </article>

            </div>
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

                </div>
                  
   
   
      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 

</body>
</html>
