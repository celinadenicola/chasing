<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>

    <!<?php include("inc/head.php"); ?>
</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="ssd">
    	<div class="container">
            <div class="col-md-10">
              <h1>Uma boa escolha</h1>
              <h2>Descubra porque o SSD da Kingston é até 10 vezes mais rápido do que um HDD.</h2>
              <p>O Kingston SSD tem muitos benefícios e vantagens quando comparado ao disco 
              rígido. Veja porque oferecer um SSD da Kingston para os seus clientes:</p>
              <p>&nbsp;</p>
              
              <img src="images/ssdKingston.png" alt="ssd Kingston" class="center">

              <h2 style="text-align: center">Kingston SSD é o melhor upgrade para qualquer computador.</h2>
                <p>&nbsp;</p>  <p>&nbsp;</p>
                <div class="col-md-12 detalles">
              <h2>Veja os gráficos comparativos e saiba porque o SSD da Kingston é a melhor escolha:</h2>
                </div>
              
               <div class="col-md-6">
               		<h3 style="color:#727277">Tempo para inicialização do Windows</h3>
               		<img src="images/columna1.png" alt="Tempo para inicializaçao do Windows">
               </div>             
                    
              <div class="col-md-6">
              	<h3 style="color:#727277">Tempo de cópia de 17,7GB de dados</h3>
              	<img src="images/columna2.png" alt="Tempo de cópia de 17,7GB de dados">
              </div>

              <div class="clearfix"></div>
              <p>&nbsp;</p>

              <div class="col-md-6">
              	<h3 style="color:#727277">Tempo para importação de 8,82GB de fotos</h3>
              	<img src="images/columna3.png" alt="Tempo para importação de 8,82GB de fotos">
              </div>

              <div class="col-md-6">
              	<h3 style="color:#727277">Tempo para importação de 20,4GB de música</h3>
              	<img src="images/columna4.png" alt="Tempo para importação de 20,4GB de música">
              </div>

              <div class="clearfix"></div>
              <p>&nbsp;</p>

               <div class="col-md-10">
              <h3>Amplo Portfólio</h3>
              <p>Os SSD da Kingston tem um <span style="color:#e31837;">amplo portfólio</span> para diferente finalidades e
              necessidades. Conheça os modelos e seus principais diferenciais:</p>
              </div>	
              <p>&nbsp;</p>

           </div>

            <div class="col-md-2" id="boxes">
            	<article class="box-celeste"><a target="_blank" href="http://www.kingston.com/br/ssd/consumer/suv400s3">
                	<h4>SSD Kingston</h4> 
                  <img src="images/ssdv300.png">
                    <p>até 10 vezes mais rápido</p> 
                  </a>
                 </article>
                 <article class="box-rosa"><a target="_blank" href="https://www.youtube.com/watch?v=IB69iHG5Uxc">
                 <p>Motivos para trocar</p>
                 <img src="images/compu.png">
                  <h4>HDD por SSD Kingston</h4>
                  </a>
              </article>
               
            </div>
            <p>&nbsp;</p>

             <!--TABLA-->
            
          <div class="col-md-10">
          	<table class="table">
          		<tr class="rojo-tr">
          			<th style="text-align: center">Modelo</th>
          			<th style="text-align: center">Capacidade</th>
          			<th style="text-align: center">Velocidade</th>
          			<th style="text-align: center">Consumidor</th>
          			<th style="text-align: center">Baixar PDF</th>
          		</tr>
          		<tr class="active">
          			<td><strong><a href="http://www.kingston.com/br/ssd/consumer/suv400s3" target="_blank">Kingston UV400</a></strong></td>
          			<td>Até 960GB</td>
          			<td>500MB/seg</td>
          			<td>Comun</td>
          			<td><a href="http://www.kingston.com/br/ssd/consumer/suv400s3" target="_blank">Baixar</a></td>
          		</tr>
          		<tr class="success">
          			<td><strong><a href="http://www.kingston.com/br/ssd/consumer/sv300s3" target="_blank">Kingston V300</a></strong></td>
          			<td>Até 480GB</td>
          			<td>450MB/seg</td>
          			<td>Comum/Empresa</td>
          			<td><a href="http://www.kingston.com/br/ssd/consumer/sv300s3" target="_blank">Baixar</a></td>
          		</tr>
          		<tr class="active">
          			<td><strong><a href="http://www.kingston.com/br/ssd/business/skc380s3" target="_blank">Kingston KC380</a></strong></td>
          			<td>Até 480GB</td>
          			<td>500MB/seg</td>
          			<td>Empresa</td>
          			<td><a href="http://www.kingston.com/br/ssd/business/skc380s3" target="_blank">Baixar</a></td>
          		</tr>
          		<tr class="success">
          			<td><strong><a href="http://www.kingston.com/br/ssd/business/skc400s37" target="_blank">Kingston KC400</a></strong></td>
          			<td>Até 1TB</td>
          			<td>550MB/seg</td>
          			<td>Empresa</td>
          			<td><a href="http://www.kingston.com/br/ssd/business/skc400s37" target="_blank">Baixar</a></td>
          		</tr>
          		
          	</table>
          <p>&nbsp;</p>
          </div>
         
              <div class="col-md-10">
             <a href="http://www.kingston.com/br/ssd/consumer/sv300s3"><img src="images/banner_SSDV300.png" class="img-responsive"></a>
             <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p>
           </div>
         <div class="clearfix">
         <p>&nbsp;</p>
          <p>&nbsp;</p>
          <p>&nbsp;</p></div>
         
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
