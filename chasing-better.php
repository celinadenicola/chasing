<!DOCTYPE html>
<html lang="br">
<head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="ESTUDIO UMO">
    <title>Kingston</title>
    <?php include("inc/head.php"); ?>

</head>

<body id="page-top" class="index">

    <!-- Navigation -->

   <?php include("includes/header2.php"); ?>

   <!--SECTION-->
    <section class="chasing-better">
    	<div class="container">
            <div class="col-md-10">
              <div class="col-md-7"><h1>Kingston está sempre em busca do melhor.</h1>
              
              <p>A nova campanha Chasing Better reforça a busca da marca em sempre fazer o
				melhor e destaca o seu <strong>amplo portfólio</strong>, que vai muito além do pendrive.<br>
				Esta inovação e renovação constante que faz da Kingston um ótimo negócio para
				quem compra e, inclusive, para quem vende.<br>
				Conheça a campanha e entenda um pouco mais porque a Kingston tem tudo para
				<strong>incrementar cada vez mais as suas vendas</strong>.</p> 
              </div>
              
              <div class="col-md-5"><img src="images/chasingbetter.png"> 
              </div>  
              <p>&nbsp;</p>
              <p>&nbsp;</p>       
              
              <div class="col-md-10"><img src="images/imagenlarga.png"> 
              </div>

              <div class="clearfix"></div>
                <p>&nbsp;</p>
                
        </div>
 
            <div class="col-md-2" id="boxes">
            	<article class="box-verde">
                 <a target="_blank" href="https://www.youtube.com/watch?v=J6Ww5g8qS-A">
                  <img src="images/logo_chasing.png" target="_blank">
                  <img src="images/videocampanha.png" target="_blank">
                  <p> Video da campanha</p> 
                 </a>
                 </article>
                 <article class="box-rosa"><a target="_blank" href=" https://youtu.be/_FummdUs1C8">
                  <h4>SSD vs HD </h4>
                  <img src="images/video.png">
                    <p>qual é o melhor?</p>
                    </a>
                 </article>
                 <article class="box-celeste"><a target="_blank" href="http://www.kingston.com/br/company/whychoosekingston">
                  <img src="images/sellokingston.png">
                  <h4>Por que Kingston?</h4> 
                  </a>
                 </article>
            </div>
            
            <!--CAJAS COLUMNAS-->
        <?php include("includes/bottom-box.php"); ?>

      </div>
            </section>
         
        <div class="clearfix"></div>

<!--FOOTER-->

    <?php include("includes/footer.php"); ?>

      <!-- jQuery -->
    
    <script src="js/modernizr-2.6.2-respond-1.1.0.min.js"></script>
    <script src="js/jquery.js"></script>
    <script src="js/jquery.easing.1.3.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.bxslider.js"></script>
    <script src="js/jquery.isotope.min.js"></script>
    <script src="js/stellar.js"></script>
    <script src="js/responsive-slider.js"></script>
    <script src="js/jquery.appear.js"></script>
    <script src="js/validate.js"></script>
    <script src="js/grid.js"></script>
    <script src="js/classie.js"></script>
    <script src="js/cbpAnimatedHeader.js"></script>
    <script src="js/agency.js"></script>

   

         <script src="js/wow.min.js"></script>
     <script>
     wow = new WOW(
     {
    
        }   ) 
        .init();
    </script>

    <script>
        $('.bxslider').bxSlider({
  minSlides: 3,
  maxSlides: 5,
  slideWidth: 170,
  slideMargin: 10
});
    </script>

    <script>
        $(window).scroll(function() {
  if ($(document).scrollTop() > 50) {
    $('nav').addClass('shrink');
  } else {
    $('nav').removeClass('shrink');
  }
})
    </script> 
</body>
</html>
